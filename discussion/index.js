// JavaScript Operators

// Arithmetic Operators

let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = x / y;
console.log("Result of division operator: " + quotient);

let remainder = y % x;
console.log("Result of modulo operator: " + remainder);

// Assignment Operators (=) - assigns the value of the right operand to a variable

let assignmentNumber = 8;

// Addition Assignment Operator (+=)

assignmentNumber = assignmentNumber + 2;
console.log("Result of addition assignment Operator: " + assignmentNumber);

// Shorthand
assignmentNumber += 2;
console.log("Result of addition assignment Operator: " + assignmentNumber);

// Subrtaction/Multiplication/Division Assignment Operator (-=, *=, /=)
assignmentNumber -= 2;
console.log("Result of subtraction assignment operator: " + assignmentNumber)
assignmentNumber *= 2;
console.log("Result of multiplication assignment operator: " + assignmentNumber)
assignmentNumber /= 2;
console.log("Result of division assignment operator: " + assignmentNumber)


// Multiple Operators and Parenthesis

let mdas = 1 + 2 -3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);

// Using Parentheses
let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas);

pemdas = (1+ (2 - 3)) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas);

// Increment and Decrement(++/--)

let z = 1;

// pre-increment
let increment = ++z;
console.log("Result of the pre-increment: " + increment);
console.log("Result of pre-increment: " + z);

// post-increment
increment = z++;
console.log("Result of the post-increment: " + increment);
console.log("Result of post-increment: " + z);

let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

decrement = z--;
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement: " + z);

// Type Coercion

let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = true + 1;
console.log(numC);

// Comparison Operators

let juan = 'juan';

// Equality Operator (==) is equal

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1'); // coercion
console.log(0 == false); // 1=true, 0=false
console.log('juan' == juan);

// Inequality operator (!=) not equal
console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1'); 
console.log(0 != false); 
console.log('juan' != juan);

// Strict Equality Operator (===) strict datatype comparison
console.log(1 === 1);
console.log(1 === 2);
console.log(1 === '1'); 
console.log(0 === false); 
console.log('juan' === juan);

// Strict Inequality operator (!==)
console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== '1'); // coercion
console.log(0 !== false); // 1=true, 0=false
console.log('juan' !== juan);

// Relational Operators
let a = 50;
let b = 65;

// Greater than (>)
let isGreaterthan = a > b;
// Less than (<)
let isLessThan = a < b;
// Greater than or equal (>=)
let isGToEqual = a >= b;
// Less than or equal (<-)
let isLTorEqual = a <= b;

console.log(isGreaterthan);
console.log(isLessThan);
console.log(isGToEqual);
console.log(isLTorEqual);

// Logical Operators
let isLegalAge = true;
let isRegistered = false;

// Logical AND Operator (&&)
// Returns true if all operands are true
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND Operator: " + allRequirementsMet);

// Logical OR Operator (||)
// Returns true if one of the operands are true
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR operator: " + someRequirementsMet);

// Logical NOT Operator (!)
// Returns the opposite value
let someRequirementsNotMet = !isRegistered;
console.log("Result of logical NOT operator: " + someRequirementsNotMet);














































































































